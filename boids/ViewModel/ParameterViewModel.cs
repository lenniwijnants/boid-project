﻿using Bindings;
using Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class ParameterViewModel
    {
    
        private RangedDoubleParameter _param;
        
        // world.Simulation.Species.ElementAt[species];
        private Cell<double> _DefaultValue { get; }
        public Cell<double> Minimum { get; set;}
        public Cell<double> Maximum { get; set; }
        public Cell<string> Id { get; set; }

        public ParameterViewModel(RangedDoubleParameter param) {
            _param = param;
            
            Minimum = Cell.Create(param.Minimum);
            Maximum = Cell.Create(param.Maximum);
            Id = Cell.Create(param.Id);
            _DefaultValue = Cell.Create(param.DefaultValue);
        }
        public Cell<double> DefaultValue
        {
            get { return Cell.Create(_param.DefaultValue); }
            set { _param.DefaultValue = value.Value; }
        }

    }

    
}
