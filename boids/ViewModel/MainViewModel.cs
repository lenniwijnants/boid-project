﻿using System.Collections.Generic;
using Model;
using Mathematics;
using Cells;
using Bindings;
using Model.Species;
using Commands;
using System;
using Services;

namespace ViewModel
{
    public class MainViewModel
    {

        public Cell<List<ParameterViewModel>> Parameters { get; }
        public Cell<bool> HunterSelected { get; set; }
        public Cell<bool> PreySelected { get; set; }
        public Cell<bool> GuardSelected { get; set; }
        public CellCommand Pause { get; set; }
        public CellCommand AddBoid { get; set; }
        public CellCommand ClearBoids { get; set; }
        public WorldViewModel world { get; set; }

        private bool paused = false;

        public MainViewModel()
        {
            world = new WorldViewModel(new Simulation());
            Parameters = Cell.Create(new List<ParameterViewModel>());

            Pause = CellCommand.FromDelegate(Cell.Create(true), pause);
            AddBoid = CellCommand.FromDelegate(Cell.Create(true), addBoid);
            ClearBoids = CellCommand.FromDelegate(Cell.Create(true), clearBoids);

            PreySelected = Cell.Create(false);
            GuardSelected = Cell.Create(false);
            HunterSelected = Cell.Create(true);

            UpdateParameters();
            
        }

        private void UpdateParameters() {
            
            Parameters.Value.Clear();
            foreach (BoidSpecies s in world.Simulation.Species)
            {

                foreach (object r in s.Bindings.Parameters)
                {
                    if (r is RangedDoubleParameter)
                    {
                        if (HunterSelected.Value)
                        {
                            if (s is HunterSpecies) Parameters.Value.Add(new ParameterViewModel((RangedDoubleParameter)r));
                            else if (PreySelected.Value)
                            {
                                if (s is PreySpecies) Parameters.Value.Add(new ParameterViewModel((RangedDoubleParameter)r));
                            }
                            else if (GuardSelected.Value)
                            {
                                if (s is GuardSpecies) Parameters.Value.Add(new ParameterViewModel((RangedDoubleParameter)r));
                            }

                        }


                    }
                }
            }
        }

        private void addBoid()
        {
            int speciesvar;
            if (HunterSelected.Value)
            {
                speciesvar = 0;
            }
            else if (GuardSelected.Value)
            {
                speciesvar = 2;
            }
            else {
                speciesvar = 1;
            }

            Random random = new Random();
            int rngX = random.Next(25, 999);
            int rngY = random.Next(25,743);
            world.Simulation.Species[speciesvar].CreateBoid(new Vector2D(rngX, rngY));
        }

        private void clearBoids() {
            world.Simulation.World.Population.Clear();
        }
        

        private void Update(double dt)
        {
            UpdateParameters();
            if (!paused) {
                world.Simulation.Update(dt);
            }
            
        }

        public void SimulationTimer_Tick(ITimerService obj)
        {
            Update(0.02);

        }

        public void pause()
        {

            if (!paused) { paused = true; }
            else { paused = false; }
        }

    }

}

