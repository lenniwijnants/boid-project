﻿using Mathematics;
using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class WorldViewModel
    {
        public Simulation Simulation { get; }

        public WorldViewModel(Simulation sim)
        {
            this.Simulation = sim;

            this.Simulation.Species[0].CreateBoid(new Vector2D(50, 50));
            this.Simulation.Species[1].CreateBoid(new Vector2D(150, 150));

            this.Simulation.Species[2].CreateBoid(new Vector2D(100, 100));

        }


    }
}
