﻿using Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bindings
{
    public class Parameter<T> : IParameter
    {
        public Parameter(string id, T defaultValue)
        {
            this.Id = id;
            this.DefaultValue = defaultValue;
        }

        

        public T DefaultValue { get; set; }

        public string Id { get; }

        object IParameter.DefaultValue
        {
            get
            {
                return DefaultValue;
            }

            set
            {
                DefaultValue = (T) value;
            }
        }

        public override string ToString()
        {
            return Id;
        }
    }

    
}
