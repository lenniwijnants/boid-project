﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ViewModel;

namespace View
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            
            var mainWindow = new MainWindow();
            mainWindow.DataContext = new MainViewModel();
            mainWindow.Show();
            var container = new UnityContainer();
            container.RegisterInstance<IUnityContainer>(container);
            container.RegisterType<ITimerService, TimerService>();
            UnityServiceLocator locator = new UnityServiceLocator(container);
            ServiceLocator.SetLocatorProvider(() => locator);
            var simtimer = ServiceLocator.Current.GetInstance<ITimerService>();
            simtimer.Tick += SimulationTimer_Tick;
            simtimer.Start(TimeSpan.FromMilliseconds(20));
        }

        private void SimulationTimer_Tick(ITimerService obj)
        {
            ((MainViewModel)MainWindow.DataContext).SimulationTimer_Tick(obj);

        }
    }
}
