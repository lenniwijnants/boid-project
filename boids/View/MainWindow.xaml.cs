﻿using Model.Species;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Threading;
using ViewModel;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void OnPause(object sender, RoutedEventArgs e) {
            if (PauseButton.Content.ToString().Equals("Pause"))
            {
                PauseButton.Content = "Resume";
            }
            else
            {
                PauseButton.Content = "Pause";
            }
        }

    }

    public class BoidToBrushConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            BoidSpecies species = value as BoidSpecies;
            string boid = species.Name;

            if (boid == null || boid.Length == 0)
            {
                return Brushes.Blue;
            }
            else if (boid.Equals("hunter"))
            {
                return Brushes.Red;
            }
            else if (boid.Equals("prey"))
            {
                return Brushes.Green;
            }
            else if (boid.Equals("guard"))
            {
                return Brushes.Yellow;
            }
            else
            {
                return Brushes.HotPink;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Console.WriteLine("Oh, oh. Spaghettio's.");

            return null;
        }
        
    }
}
